# Flight from a Moving Vehicle
**Manual Mission**
<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/moving_vehicle_project/11_11_2023_manual_mission.mp4/" type="video/mp4">
</video>

**Waypoint Mission**
<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/moving_vehicle_project/11_11_2023_waypoint_mission.mp4/" type="video/mp4">
</video>

<!-- Meta Data Table Starts -->
<h4><b><u>Meta Data</b></u></h4>
<style>
table {
  border-collapse: collapse;
}
td,
th {
  border: 1px solid black;
  padding: 10px 20px;
}
</style>
<table>
  <tr>
    <td><b>Date</b>.</td>
    <td>11/11/2023</td>
  </tr>
  <tr>
    <td><b>Aircraft</b>.</td>
    <td>DJI M210</td>
  </tr>
  <tr>
    <td><b>Payload</b></td>
    <td>Z30 RGB with Zoom Sensor</td>
  </tr>
  <tr>
    <td><b>Altitude</b></td>
    <td>150ft</td>
  </tr>
  <tr>
    <td><b>Airspace</br></td>
    <td>Class G</td>
  </tr>
</table>
<!-- Meta Data Table Ends -->

### Objectives:
The purpose of this lab was to gain experience conducting UAV operations from a moving vehicle. We conducted two separate operations: a manual mission and a way point mission. Both presented its own challenges but required the same basic set up so we were able to complete both during the same flight outing.
### Preflight Preparations:
In order to comply with FAA regulations, we had to fly in a sparsely populated area. We used GIS methods to select a an area which was sparsely populated and had obstacles no higher than ~150ft. We were also careful to plan out our route of travel so that we wouldn't encounter obstacles which posed a threat to our VLOS needs.
### Hazard Assessment and Risk Controls:
The biggest challenge that we faced with this lab was maintaining VLOS with the drone. Before we started flying, we knew this would be a hazard. If we drove too close (or under) the drone, we risked having the car itself impair our VLOS. If we drove too far from the drone, we risked losing VLOS and communications with the drone. To address this, we created a simple communication system which would allow the PIC and VO to efficiently and effectively communicate to the driver to either speed up, maintain, slow down, or stop.
### Takeaways:
From this flight outing we learned that UAV operations from a moving vehicle can be conducted safely and effectively but additional planing and precautions are required. Operations from a moving vehicle could be necessary if the flight area presents VLOS hazards. 
### Deliverables:
The full hd/bitrate waypoint mission can be viewed [here](https://purdue0-my.sharepoint.com/personal/reilly28_purdue_edu/_layouts/15/stream.aspx?id=%2Fpersonal%2Freilly28%5Fpurdue%5Fedu%2FDocuments%2Fmkdocs%5Fwebsite%2Fmkdocs%5Flinks%2F11%5F11%5F2023%5Fwaypoint%5Fmission%2Emp4&referrer=StreamWebApp%2EWeb&referrerScenario=AddressBarCopied%2Eview)

The full hd/bitrate manual mission can be viewed [here](https://purdue0-my.sharepoint.com/personal/reilly28_purdue_edu/_layouts/15/stream.aspx?id=%2Fpersonal%2Freilly28%5Fpurdue%5Fedu%2FDocuments%2Fmkdocs%5Fwebsite%2Fmkdocs%5Flinks%2F11%5F11%5F2023%5Fmanual%5Fmission%2Emp4&referrer=StreamWebApp%2EWeb&referrerScenario=AddressBarCopied%2Eview)
