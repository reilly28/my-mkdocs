# Night Flight at Rose-Ade

<!-- Meta Data Table Starts -->
<h4><b><u>Meta Data</b></u></h4>
<style>
table {
  border-collapse: collapse;
}
td,
th {
  border: 1px solid black;
  padding: 10px 20px;
}
</style>
<table>
  <tr>
    <td><b>Date</b>.</td>
    <td>11/14/2023</td>
  </tr>
  <tr>
    <td><b>Aircraft</b>.</td>
    <td>DJI M210</td>
  </tr>
  <tr>
    <td><b>Payload</b></td>
    <td>XT2 Thermal/RGB Sensor</td>
  </tr>
  <tr>
    <td><b>Altitude</b></td>
    <td>Below 200ft</td>
  </tr>
  <tr>
    <td><b>Airspace</br></td>
    <td>Class D</td>
  </tr>

</table>
<!-- Meta Data Table Ends -->

### Objectives:
The purpose of this mission was to gain experience conducting night operations and utilizing a thermal sensor for practical applications. We practiced basic flight operations and locating and tracking subjects. 

### Preflight Preparations:

Preparations for this operation were relatively simply. We applied for and were granted a LAANC approval to operation in the KLAF class D airspace and at night. Before heading to the sight we made double sure that our beacon was operational.

### Hazard Assessment and Risk Controls:

1. **Flight Over People and Vehicles** - As always this is a major concern for us whenever operation on campus. This risks increase when flying at night as it becomes more difficult to see and identify people and vehicles which may enter our operational area. To address this, we tasked the sensor operator with identifying potential hazards. He would identify and communicate both to the PIC and VO.
2. **Maintaining VLOS** - Another hazard which is magnified when flying at night is maintaining visual line of sight with the aircraft. Even though the use of a beacon is required in part 107 operations, we quickly found out that it would be very easy to drift behind a tree or building. These can blend in with the night sky and server VLOS. To address this, we made sure that at all times we could clearly see the beacon on the aircraft. 

### Takeaways:

From this flight outing we learned about the additional hazards and risks associated with flying at night. Maintaining VLOS, obstacle avoidance, and overall hazard detection and contingency events becomes much more difficult to deal with. 

### Deliverables:

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Image Slideshow</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f1f1f1;
        }

        .slideshow-container {
            max-width: 800px;
            position: relative;
            margin: auto;
            overflow: hidden;
        }

        .mySlides {
            display: none;
            width: 100%;
            transition: opacity 1s ease-in-out;
        }

        img {
            width: 100%;
            height: auto;
        }

        .fade {
            animation: fade 10s infinite;
        }

        @keyframes fade {
            0%, 100% {
                opacity: 0;
            }
            40%, 90% {
                opacity: 1;
            }
        }
    </style>
</head>
<body>

<div class="slideshow-container">
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_1.png" alt="Image 1" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_2.png" alt="Image 2" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_3.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_4.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_5.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_6.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_7.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_8.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_9.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_10.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_11.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_12.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_13.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_14.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_15.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <div class="mySlides fade">
        <img src="../content/night_flight_project/nf_16.png" alt="Image 3" style="border: 5px solid #333; border-radius: 5px;">
    </div>
    <!-- Add more slides as needed -->

    <!-- Optional: Add navigation buttons -->
    <!--<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>-->
</div>

<!-- Optional: Add dots for slide indicators -->
<!--<div style="text-align:center">
    <span class="dot"></span>
    <span class="dot"></span>
    <span class="dot"></span>
</div>-->

<script>
    let slideIndex = 0;
    showSlides();

    function showSlides() {
        let i;
        const slides = document.getElementsByClassName("mySlides");

        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }

        slideIndex++;

        if (slideIndex > slides.length) {
            slideIndex = 1;
        }

        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 10000); // Change slide every 5 seconds
    }
</script>

</body>
</html>
