# Title

<!-- Meta Data Table Starts -->
<h4><b><u>Meta Data</b></u></h4>
<style>
table {
  border-collapse: collapse;
}
td,
th {
  border: 1px solid black;
  padding: 10px 20px;
}
</style>
<table>
  <tr>
    <td><b>Aircraft</b>.</td>
    <td>cell 2</td>
  </tr>
  <tr>
    <td><b>Payload</b></td>
    <td>Cell 2.</td>
  </tr>
  <tr>
    <td><b>Altitude</b></td>
    <td>Cell2</td>
  </tr>
  <tr>
    <td><b>Airspace</br></td>
    <td>Cell2</td>
  </tr>

</table>
<!-- Meta Data Table Ends -->

### Objectives:

### Preflight Preparations:

### Hazard Assessment and Risk Controls:

### Takeaways:

### Deliverables:
