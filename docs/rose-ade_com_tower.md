# Rose-Ade Communication Tower


<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/rose-ade_tower_project/m210_tower_flythrough_edited.mp4" type="video/mp4">
</video>

<!-- Meta Data Table Starts -->
<h4><b><u>Meta Data</b></u></h4>
<style>
table {
  border-collapse: collapse;
}
td,
th {
  border: 1px solid black;
  padding: 10px 20px;
}
</style>
<table>
  <tr>
    <td><b>Date</b>.</td>
    <td>10/03/2023</td>
  </tr>
  <tr>
    <td><b>Aircraft</b>.</td>
    <td>DJI M210</td>
  </tr>
  <tr>
    <td><b>Payload</b></td>
    <td>X7 RGB Sensor</td>
  </tr>
  <tr>
    <td><b>Altitude</b></td>
    <td>Below 200ft</td>
  </tr>
  <tr>
    <td><b>Airspace</br></td>
    <td>Class D</td>
  </tr>
</table>
<!-- Meta Data Table Ends -->

### Objectives:
The purpose of this lab was to obtain aerial imagery of a building for a simulated general contractor. The final output was to be a 3d model of the Purdue Rose-Ade Boiler communication tower. Our output, which can be seen above, is a point cloud mesh from which a 3d model can be generated. 

### Preflight Preparations:
Preparations for this flight were relatively easy. As can be seen below, this mission took place within a 200ft block of the KLAF class D airspace. This meant that the only approval needed to fly at this location was a LAANC which we obtained through *Air Control*. 


<img src="../content/rose-ade_tower_project/facility_maps_ss.png" width="700" height="700" style="border: 5px solid #333; border-radius: 5px;">

### Hazard Assessment and Risk Controls:
In our initial hazard assessment we were most concerned about flight over people/vehicles and maintaining safe separation with manned traffic. 

1. **Flight over people/vehicles** - The communication tower is located right next to the Purdue golf course which meant that there was a possibility of golfers or golf course staff entering our area of operation. Additionally, the communication tower has a small gravel parking lot. This also presented a possibility for people or vehicles unexpectedly entering the area of operation. To address these risks, we designated one of our crew members (3 total) to be responsible for maintaining an operational perimeter when the UAV was in the air. 

2. **Maintaining proper separation with manned traffic** - This was one of our biggest concerns because the tower is located only 1.7 nm away from KLAF airport. The tower is also situated in parallel with runway 28/10. This meant our operational area would be located under the downwind leg of the VFR traffic pattern. Standard VFR traffic pattern standards recommend pilots to be ~1000ft agl while on the downwind. With the tower being 160ft tall and our maximum altitude being 200ft, we were comfortable with an 800ft separation between manned traffic in the event of a direct fly over. 

### Takeaways:
From this operation, our group learned how important it is to be prepared for manned aerial traffic. Even though we did our homework well and knew that we should have at minimum an 800 ft separation, this ended up not being the case. During the operation, we had multiple incidents of direct flyovers with separations of less than 500 ft. One thing our group could have done was to utilize a hand-held VHF radio. This would have given us better preparation for incoming traffic on the downwind and would have given us much more time to pause our operations. 


### Deliverables:

This 3D model is our finished product from the assignment. 
<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/rose-ade_tower_project/m210_tower_flythrough_edited.mp4" type="video/mp4">
</video>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="../css_stuff/style.css">
        <link href="https://fonts.googleapis.com/css?family=Karla" rel="stylesheet">
        <title>slide image tutorial</title>
    </head>
    <body>
        <h5>Extra Photos</h5>
        <div class="slider-frame">
            <div class="slide-images">
                    <div class="img-container">
                        <img src="../content/rose-ade_tower_project/rose_ade_2.JPG" width="750" style="border: 5px solid #333; border-radius: 5px;">
                    </div>
                    <div class="img-container">
                        <img src="../content/rose-ade_tower_project/The3.JPG" width="750" style="border: 5px solid #333; border-radius: 5px;">
                    </div>
                    <div class="img-container">
                        <img src="../content/rose-ade_tower_project/Tower_3D_ScreenShot.PNG" style="border: 5px solid #333; border-radius: 5px;">
                    </div>
            </div>
        </div>
    </body>
</html>
