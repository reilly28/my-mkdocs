# NSF-EAGER Campaign  
<em>Please follow this [link](https://de-wekker-lab.gitlab.io/eager/) to learn more about our research trip to Tonopah, NV.</em>  
### Overview:
From September 10th to the 16th of 2023, Purdue faculty and staff specializing in UAV operations were given the opportunity to participate in one of the National Science Foundations EAGER campaigns, located near the Tonopah airport in Nevada.  
<br>
<img src="../content/nevada_trip/nevada_5.jpeg" alt="Your Image Description" style="border: 5px solid #333; border-radius: 5px;">
<br>

### Purpose:
The goal of this research trip was to compare barometric, IMU, and positional data, collected with DJI drones, with the very sensitive and accurate array of sonic anemometers which were already set up by the NSF (anemometers pictured below).  

<img src="../content/nevada_trip/nevada_2.jpg" width='900' style="border: 5px solid #333; border-radius: 5px;">  
<img src="../content/nevada_trip/nevada_4.jpg" width='900' style="border: 5px solid #333; border-radius: 5px;">  
<img src="../content/nevada_trip/nevada_3.jpg" width='900' style="border: 5px solid #333; border-radius: 5px;">  
<img src="../content/nevada_trip/nevada_1.jpg" width='900' style="border: 5px solid #333; border-radius: 5px;">  

### Results:

Overall, this research trip was very successful. We were able to gather roughly 4 hours worth data per drone each day with 8 drones. This provided students with real world experience and gave us plenty of data to work with which is what were are currently focused on.  
<br>  

<em>Please follow this [link](https://de-wekker-lab.gitlab.io/eager/) to learn more about our research trip to Tonopah, NV.</em>
