# PUSH and RHPH Survey

<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/push_pharm_project/push_flythrough_edited.mp4" type="video/mp4">
</video>

<!-- Meta Data Table Starts -->
<h4><b><u>Meta Data</b></u></h4>
<style>
table {
  border-collapse: collapse;
}
td,
th {
  border: 1px solid black;
  padding: 10px 20px;
}
</style>
<table>
  <tr>
    <td><b>Date</b>.</td>
    <td>10/28/2023</td>
  </tr>
  <tr>
    <td><b>Aircraft</b>.</td>
    <td>DJI M210</td>
  </tr>
  <tr>
    <td><b>Payload</b></td>
    <td>XT2 Thermal/RGB Sensor</td>
  </tr>
  <tr>
    <td><b>Altitude</b></td>
    <td>150ft</td>
  </tr>
  <tr>
    <td><b>Airspace</br></td>
    <td>Class D</td>
  </tr>

</table>
<!-- Meta Data Table Ends -->

### Objectives:
In this project we partnered with civil engineering students to obtain RGB and thermal 3D models of the PUSH and RHPH buildings on Purdue's campus. The main goals were to obtain an airspace authorization from the FAA and get practice flying in areas which are more urbanized. 

### Preflight Preparations:
The most challenging and time consuming component of this flight was the FAA airspace authorization.

<img src="../content/push_pharm_project/uas_facilities_map.png" style="width:370px;height:400px; border: 5px solid #333; border-radius: 5px;"> <img src="../content/push_pharm_project/google_maps_ss.png" style="width:370px;height:400px; border: 5px solid #333; border-radius: 5px;">

As can be seen from the image above, our flight operations area is located within a zero block. This means that an airspace authorization could not be obtained from LAANC providers. Instead, we had to obtain a COA (Certificate of Waiver or Authorization) from the FAA in order to conduct any kind of UAV operations in this block. 

**The FAA approved our request and we were issued a CAO which can be viewed <a href="../content/push_pharm_project/FAA_COA.pdf">here</a>.**

The COA states that during operations, we must be available for direct contact from KLAF ATC. It also states that list must be maintained which contains any remote pilot's name and pilot certificate who is associated with the operation. This list must be accessible at all times and will be presented upon request from an authorized representative. Additionally, we must contact KLAF ATC directly by phone at least 30 mins prior to flying and immediately after the operation ends. The final rule we needed to follow was specific contingency plans. In the event of a loss of link or loss of GPS, the UAV must be configured to return to home. 

### Hazard Assessment and Risk Controls:

1. **Flight over people/vehicles** - To make a 3D model of the PUSH and RHPH buildings, we needed to fly an automated mission which would last roughly 10 min. This means that any portion of our flight can not be considered transient if we were to fly over people. Sustained flight over people would require additional FAA approval which we did not have. Therefore, it was critical to keep pedestrians out of the operations area. To achieve this, we set the speed of the mission to <5 mph and walked with the drone in its direction of travel. This allowed us to more easily detect and assess which pedestrians needed to be guided away during the operation. The other hazard was N University St which ran up against the west side of both buildings. 

<img src='../content/push_pharm_project/google_maps_ss_2.png' width="600" style="border: 5px solid #333; border-radius: 5px;">

To avoid flying over the road we only had to make modifications to our mission settings. Additional care was taken when the drone was flying near this area in case we had to assume manual control of the UAV to navigate back to a safe position. 

### Takeaways:
Our group's greatest struggle with completing this operation was ensuring that no sustained flight over people or vehicles took place. To maintain adequate separation between the UAV and pedestrians/vehicles, the flight had to be paused multiple times. It made the operation more difficult and much longer than we had anticipated. This was a good learning lesson however. In future operations like this one, we will be sure to implement more preflight preparations aimed at mitigating pedestrian involvement in operations areas.

### Deliverables:

<video width="1920" height="1080" autoplay muted loop style="border: 5px solid #333; border-radius: 5px;">
  <source src="../content/push_pharm_project/push_flythrough_edited.mp4" type="video/mp4">
</video>
