# Maps and Figures

<head>
<style>
.img_container {
  background-color: red;
  height: 100%;
  width: 1200px;
  padding-right: 15px;
  padding-left: 15px;
  padding-top: 15px;
  padding-bottom: 15px;
}
.maps {
  position: relative;
  border: 5px solid rgb(0, 0, 0);
  border-radius: 25px;
  width: 33%;
}
.maps:hover {
  border: 7px solid blue;
}
.heading_text {
  font-size: 24;
}
</style>

<p>Below is a series of deliverables from various missions and flight outings I conducted while studying at <b>Purdue University</b>:</p>

<div class=img_container>
    <img src='../content/maps_figures/7.png' class=maps>
    <img src='../content/maps_figures/DJI_0026.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0027.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0028.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0032.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0033.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0034.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0035.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0036.JPG' class=maps>
    <img src='../content/maps_figures/DJI_0037.JPG' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
    <img src='../content/maps_figures/' class=maps>
</div>
</head>
