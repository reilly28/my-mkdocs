# Education
### <ins>**Purdue University:**</ins>
Expected graduation, May 2024 <img src='../content/education_project/purdue_seal.png' style="position:absolute; left:950px; top:100px; width:300px; height:300px;">  
Bachelor of Science in Unmanned Aerial Systems  
GPA [4.0 scale]: Major 3.4  
Overall GPA: 3.2  

### <b>Relevant Course Work</b>
* Aerospace Vehicle Propulsion & Tracking
* UAS Design and Construction
* UAS Inspection and Repair
* Aerospace Vehicle Systems
* Unmanned Autonomous Aerial Systems
* Aircraft Electrical Systems
* Avionics Systems
* Managerial Economics in Aviation

