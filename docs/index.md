# Aaron Reilly

<head>
<style>
.img_container {
    float: right;
    position: relative; 
    display: flex;
    height: 300px;
    width: 300px;
    margin-left: 10px;
    margin-right: 400px;
    overflow: hidden;
}
.img_container_1 {
    position: absolute;
    left: 0;
    top: 10px;
    width: 100%;
    height: 90%;
    border: 5px solid rgb(0, 0, 0);
    border-radius: 400px;
    overflow: hidden;
}
.background_img {
    position: relative;
    width: 1220px;
    height: 800px;
    background-image: url('../content/random_photos/tonopah_sky.jpeg');
    border-radius: 25px;
}
.text_overlay {
    position: absolute;
    margin-left: 10px;
    background-color: rgba(191, 157, 157, .6);
    margin-right: 750px;
    padding-left: 15px;
    border-radius: 25px;
    padding-top: 10px;
    padding-bottom: 10px;
}
.img_1 {
    position: absolute;
    size: 150%;
    top: 0;
}

</style>
<body>
    <div class=background_img>
        <div class=img_container>
            <div class=img_container_1>
                <img src='../content/random_photos/headshot.jpg' class=img_1>
            </div>
        </div>
        <p class=text_overlay>
            Hello! My name is Aaron Reilly. I am currently a senior at Purdue University studying Unmanned Aerial Systems. Here, you will find information and examples regarding my education, skills, and qualifications. 
            <br>
            <ins><b>Professional Goal</b></ins> 
            <br> 
            - To gain experience in industries utilizing UAVs to solve problems and provide essential services.
            <br>
        </p>
    </div>    
</body>
</head>

